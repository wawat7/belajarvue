<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::latest()->paginate(5);
        return response()->json($categories, 200);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'=> 'required|string|max:50',
            'image' => 'required|image|mimes:jpeg,png,jpg'
        ]);

        try {
            #move file photo
            $filePhoto = "Photo/Category/";
            $photo = $request->file('image');
            $photoName = time().".".$photo->getClientOriginalExtension();
            $photo->move(public_path($filePhoto), $photoName);


            #save category
            $category = Category::create(['name' => $request->name, 'image' => $filePhoto.$photoName]);

            return response()->json($category, 200);
        } catch (\Throwable $th) {
            return response()->json($th->getMessage(), 500);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category)
    {
        $request->validate([
            'name'=> 'required|string|max:50',
        ]);

        try {
            #save category
            $category->name = $request->name;

            if ($request->hasFile('image')) {
                $request->validate([
                    'image' => 'image|mimes:jpeg,png,jpg'
                ]);
                #remove photo before
                unlink(public_path().'/'.$category->image);

                $filePhoto = "Photo/Category/";
                $photo = $request->file('image');
                $photoName = time().".".$photo->getClientOriginalExtension();
                $photo->move(public_path($filePhoto), $photoName);
                $category->image = $filePhoto.$photoName;
            }

            $category->save();
            return response()->json($category, 200);

        } catch (\Throwable $th) {
            return response()->json($th->getMessage(), 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        #remove photo
        unlink(public_path().'/'.$category->image);

        if ($category->delete()) {
            return response()->json([
                'message' => 'category has been deleted',
                'status_code' => 200
            ], 200);
        }else{
            return response()->json([
                'message' => 'Something when wrong..',
                'status_code' => 500
            ], 500);
        }
    }
}
