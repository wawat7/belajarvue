import Vue from 'vue';
import Router from 'vue-router';
import Welcome from './views/Welcome.vue';
import Category from './views/Category.vue';

Vue.use(Router);

const routes = [
    {
        path: '/',
        name: 'welcome',
        component: Welcome
    },
    {
        path: '/categories',
        name: 'categories',
        component: Category
    }
];

const router = new Router({
    routes: routes,
    linkActiveClass: 'active'
});

export default router;